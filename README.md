# Projet LIDE

Le but de ce projet est de créer un environnement de développement en ligne dédié aux étudiants de licence.
Cette application leur permettra d’éviter le téléchargement de tous les éditeurs de code (complexes) nécessaires dans le cadre des `Cours`, `TP` et `Projets`.

L'interface devra être sobre et plus simple pour ne pas déstabiliser les étudiants débutants (par exemple ceux de la 1<sup>ère</sup> année de licence).

## Technologies

Ce projet est développé principalement autour des frameworks :
- [Symfony](https://symfony.com/) en back-end;
- [VueJS](https://vuejs.org/) en front-end.
Bases de données : [MariaDB](https://mariadb.org/).

L'application `LIDE`, dans sa globalité, est déployé dans des conteneurs [Docker](https://www.docker.com/).

## Symfony + VueJS

Pour comprendre comment on procède pour intégrer du `VueJS` dans un projet `Symfony` : [Symfony + vuejs](https://blog.dev-web.io/2018/01/11/symfony-4-utiliser-vue-js/).

## Installation

Avant d'exécuter l'application, il faut s'assurer que `Docker` est installé sur votre poste.
Si ce n'est pas le cas, référez vous à la [documentation officielle](https://docs.docker.com/get-docker/) de Docker pour procéder à son installation.

Le script Bash `installer.sh` fait à la fois la création des conteneurs (`WEB` et `PMA`) et le lancement de ces conteneurs.
```bash
./installer.sh web pma

# Les options 'web', 'pma'
# web : Serveur web de l'application - intéractions avec l'utilisateur
# pma : Projects Manager Application -> une API de gestion de répertoires et fichiers de codes source des utilisateurs.
```

Si les conteneurs sont déjà créés, il faut les lancer avec le script `lancer-conteneurs.sh`.
```bash
./lancer-conteneurs.sh # sans options
```

Une fois l'installation et le lancement terminé, l'application sera accessible sur [localhost:9000](http://localhost:9000).