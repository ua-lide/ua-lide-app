# UA-LIDE
## LideProjectManager - Migration Symfony4

### Version 1.0.0
* updated configs
* updated tests
* fonctionning routes
* TODO: fix jwt

#### Version 0.3
* updated routes, services
* symfony 4

#### Version 0.2
* update to flex project hierarchy

#### Version 0.1
* init using sources from lidepma (symfony 3)