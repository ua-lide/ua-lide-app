#!/bin/bash

# Ce script fait l'installation des deux conteneurs Docker,
# WEB et PMA, nécessaires pour avoir l'infrastructure complète de
# l'application LIDE.
# Le conteneur WEB : Application WEB, intéractions avec l'utilisateur
# Le conteneur PMA : Projects Manager Application -> Gestion des répertoires et fichiers
# de codes sources des utilisateurs.

IMAGE_PMA_NAME=lide-pma
IMAGE_WEB_NAME=lide-web
CONTAINER_PMA_NAME=lide-pma
CONTAINER_WEB_NAME=lide-web


if [ "$1" = "web" ] || [ "$2" = "web" ]
then

	echo -e "\033[33m>>>Arrêt du conteneur 'lide-web' ... \033[0m" &&
	docker stop $CONTAINER_WEB_NAME 2>/dev/null;

	echo -e "\033[33m>>>Création de l'image 'lide-web' (serveur web) ... \033[0m" &&
	docker build ./lide-web -t $IMAGE_WEB_NAME &&
	echo -e "\033[33m>>>Lancement d'un conteneur 'lide-web'... \033[0m" &&
	docker run --name $CONTAINER_WEB_NAME --rm -d -p 9000:9000/tcp $IMAGE_WEB_NAME:latest  ||

	echo -e "\033[31m>>>ERREUR lors de la création de l'image lide-web !\033[0m" 
fi


if [ "$1" = "pma" ] || [ "$2" = "pma" ]
then

	echo -e "\033[33m>>>Arrêt du conteneur 'lide-pma' ... \033[0m" &&
	docker stop $CONTAINER_PMA_NAME 2>/dev/null;

	echo -e "\033[33m>>>Création de l'image 'lide-pma' (serveur web) ... \033[0m" &&
	docker build ./lide-pma -t $IMAGE_PMA_NAME &&
	echo -e "\033[33m>>>Lancement d'un conteneur 'lide-pma'... \033[0m" &&
	docker run --name $CONTAINER_PMA_NAME --rm -d -p 7000:7000/tcp -p 7001:7001/tcp -v /var/run/docker.sock:/var/run/docker.sock  -v /lide_storage:/lide_storage $IMAGE_PMA_NAME:latest ||

 	echo -e "\033[31m \nERREUR lors de la création de l'image lide-web !\033[0m" 
fi