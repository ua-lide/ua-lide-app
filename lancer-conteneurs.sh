#!/bin/bash

IMAGE_PMA_NAME=lide-pma
IMAGE_WEB_NAME=lide-web
CONTAINER_PMA_NAME=lide-pma
CONTAINER_WEB_NAME=lide-web

echo -e "\033[33m>>>Arrêt du conteneur 'lide-web' ... \033[0m" &&
	docker stop $CONTAINER_WEB_NAME 2>/dev/null;

echo -e "\033[33m>>>Lancement d'un conteneur 'lide-web'... \033[0m" &&
	docker run --name $CONTAINER_WEB_NAME --rm -d -p 9000:9000/tcp $IMAGE_WEB_NAME:latest

echo -e "\033[33m>>>Arrêt du conteneur 'lide-pma' ... \033[0m" &&
	docker stop $CONTAINER_PMA_NAME 2>/dev/null;

echo -e "\033[33m>>>Lancement d'un conteneur 'lide-pma'... \033[0m" &&
	docker run --name $CONTAINER_PMA_NAME --rm -d -p 7000:7000/tcp -p 7001:7001/tcp -v /var/run/docker.sock:/var/run/docker.sock  -v /lide_storage:/lide_storage $IMAGE_PMA_NAME:latest 
